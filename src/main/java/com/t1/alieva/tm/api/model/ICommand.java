package com.t1.alieva.tm.api.model;

import com.t1.alieva.tm.exception.entity.AbstractEntityNotFoundException;
import com.t1.alieva.tm.exception.field.AbstractFieldException;
import com.t1.alieva.tm.exception.user.AbstractUserException;
import com.t1.alieva.tm.exception.user.AuthenticException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.naming.AuthenticationException;
import java.io.FileNotFoundException;
import java.io.IOException;


public interface ICommand {

    String toString();

    @NotNull
    String getName();

    @Nullable
    String getArgument();

    @NotNull
    String getDescription();

    void execute() throws AbstractEntityNotFoundException, AbstractFieldException, AbstractUserException, AuthenticationException, AuthenticException, IOException, ClassNotFoundException;
}
