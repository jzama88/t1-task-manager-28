package com.t1.alieva.tm.command.system;

import com.t1.alieva.tm.api.model.ICommand;
import com.t1.alieva.tm.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;

public final class ApplicationHelpCommand extends AbstractSystemCommand {

    @Override
    @NotNull
    public String getName() {
        return "help";
    }

    @Override
    @NotNull
    public String getArgument() {
        return "-h";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Show application commands.";
    }

    @Override
    public void execute() {
        System.out.println("[HELP]");
        @Nullable final Collection<AbstractCommand> commands = getCommandService().getTerminalCommands();
        for (@NotNull final ICommand command : commands) System.out.println(command);
    }
}
