package com.t1.alieva.tm.command.system;

import com.t1.alieva.tm.api.model.ICommand;
import com.t1.alieva.tm.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;

public final class ArgumentListCommand extends AbstractSystemCommand {

    @Override
    @NotNull
    public String getName() {
        return "arguments";
    }

    @Override
    @NotNull
    public String getArgument() {
        return "arg";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Show application arguments.";
    }

    @Override
    public void execute() {
        System.out.println("ARGUMENTS");
        @Nullable final Collection<AbstractCommand> commands = getCommandService().getTerminalCommands();
        for (@NotNull final ICommand command : commands) {
            @Nullable final String name = command.getName();
            if (name == null || name.isEmpty()) continue;
            System.out.println(name);
        }
    }
}
