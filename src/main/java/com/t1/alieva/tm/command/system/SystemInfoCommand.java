package com.t1.alieva.tm.command.system;

import com.t1.alieva.tm.util.FormatUtil;
import org.jetbrains.annotations.NotNull;

public final class SystemInfoCommand extends AbstractSystemCommand {

    @Override
    @NotNull
    public String getName() {
        return "info";
    }

    @Override
    @NotNull
    public String getArgument() {
        return "-i";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Show System Info";
    }

    @Override
    public void execute() {
        @NotNull final Integer availableProcessors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors (cores): " + availableProcessors);
        @NotNull final Long freeMemory = Runtime.getRuntime().freeMemory();
        @NotNull final String freeMemoryFormat = FormatUtil.formatBytes(freeMemory);
        System.out.println("Free memory (bytes): " + freeMemoryFormat);
        @NotNull final Long maxMemory = Runtime.getRuntime().maxMemory();
        @NotNull final String maxMemoryValue = FormatUtil.formatBytes(maxMemory);
        @NotNull final Boolean maxMemoryCheck = maxMemory == Long.MAX_VALUE;
        @NotNull final String maxMemoryFormat = maxMemoryCheck ? "no limit" : maxMemoryValue;
        System.out.println("Maximum memory (bytes): " + maxMemoryFormat);
        @NotNull final Long totalMemory = Runtime.getRuntime().totalMemory();
        @NotNull final String totalMemoryFormat = FormatUtil.formatBytes(totalMemory);
        System.out.println("Total memory (bytes): " + totalMemoryFormat);
        @NotNull final Long usageMemory = totalMemory - freeMemory;
        @NotNull final String usageMemoryFormat = FormatUtil.formatBytes(usageMemory);
        System.out.println("Usage memory (bytes): " + usageMemoryFormat);
    }
}
