package com.t1.alieva.tm.command.task;

import com.t1.alieva.tm.exception.entity.AbstractEntityNotFoundException;
import com.t1.alieva.tm.exception.field.AbstractFieldException;
import com.t1.alieva.tm.exception.user.AbstractUserException;
import com.t1.alieva.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;

public final class TaskCreateCommand extends AbstractTaskCommand {

    @Override
    @NotNull
    public String getName() {
        return "task-create";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Create new task.";
    }

    @Override
    public void execute() throws
            AbstractEntityNotFoundException,
            AbstractFieldException,
            AbstractUserException {
        System.out.println("[CREATE TASK]");
        System.out.println("ENTER TASK NAME:");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("ENTER TASK DESCRIPTION:");
        @NotNull final String description = TerminalUtil.nextLine();
        @NotNull final String userId = getUserId();
        getTaskService().create(userId, name, description);
    }
}
