package com.t1.alieva.tm.command.user;

import com.t1.alieva.tm.enumerated.Role;
import com.t1.alieva.tm.exception.entity.AbstractEntityNotFoundException;
import com.t1.alieva.tm.exception.field.AbstractFieldException;
import com.t1.alieva.tm.exception.user.AbstractUserException;
import com.t1.alieva.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;

public class UserUnlockCommand extends AbstractUserCommand {

    @Override
    public void execute() throws
            AbstractUserException,
            AbstractFieldException,
            AbstractEntityNotFoundException {
        System.out.println("[USER UNLOCK]");
        System.out.println("ENTER LOGIN:");
        @NotNull final String login = TerminalUtil.nextLine();
        getUserService().unlockUserByLogin(login);
    }

    @Override
    @NotNull
    public String getDescription() {
        return "user unlock";
    }

    @Override
    @NotNull
    public String getName() {
        return "user-unlock";
    }

    @Override
    @NotNull
    public Role[] getRoles() {
        return new Role[]
                {
                        Role.ADMIN
                };
    }
}
