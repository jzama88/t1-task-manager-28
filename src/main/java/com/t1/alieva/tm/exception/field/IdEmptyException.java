package com.t1.alieva.tm.exception.field;

public final class IdEmptyException extends AbstractFieldException {

    public IdEmptyException() {
        super("Error! ID is empty...");
    }
}
